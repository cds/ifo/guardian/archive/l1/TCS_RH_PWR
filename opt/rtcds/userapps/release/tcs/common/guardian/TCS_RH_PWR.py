from guardian import GuardState
import time

## FUNCTIONS

def adj_rh_power(initial_power , diff):
	ezca['UPPERPOWER'] = initial_power + diff
	ezca['LOWERPOWER'] = initial_power + diff

## insert error checker


 ## STATES ##

class NOMINAL_RH_INPUT(GuardState):
        index = 3
        def main(self):
                ezca.switch('INVERSE_FILTER', 'FM1', 'OFF')                       #turn off the RH filter
                ezca['INVERSE_FILTER_RSET'] = 2                                   #clear filter history
                ezca['INVERSE_FILTER_GAIN'] = 0
                ezca['FILTER_IN'] = 0

class UNFILTERED_RH_INPUT(GuardState):
        index = 1
        def main(self):
                diff_upper = 0
                diff_lower = 0
                self.UPPER = ezca['SETUPPERPOWER']
                self.LOWER = ezca['SETLOWERPOWER']

        def run(self):
                diff_upper = ezca['SETUPPERPOWER'] - self.UPPER
                diff_lower = ezca['SETLOWERPOWER'] - self.LOWER
                if (diff_upper + diff_lower) > 0:
                        if abs(diff_upper) > 0:                                            
                                self.diff = diff_upper
                                self.init_pow = ezca['SETLOWERPOWER']
                        else:
                                self.diff = diff_lower
                                self.init_pow = ezca['SETUPPERPOWER']
                        adj_rh_power(self.init_pow, self.diff)                             # adjust both segments
                return True


class FILTER_RH_INPUT(GuardState):
        index = 2
        def main(self):
                diff_upper = 0
                diff_lower = 0
                self.UPPER = ezca['SETUPPERPOWER']
                self.LOWER = ezca['SETLOWERPOWER']
                self.timer['wait'] = 600

                while ((diff_upper and diff_lower) == 0) or self.timer['wait']:    #waits for a change to be made on the upper or lower segment of TM RH (expires after 10 minutes)
                        diff_upper = ezca['SETUPPERPOWER'] - self.UPPER
                        diff_lower = ezca['SETLOWERPOWER'] - self.LOWER
                        time.sleep(3)

                if self.timer['wait']:                                             # returns you to NOMINAL_RH_input if no power change was made after 10 minutes
                        #return to NOMINAL_RH_input state
                        blah = 1

                if (abs(diff_upper) > 0):                                         
                        self.diff = diff_upper
                        self.init_pow = ezca['SETLOWERPOWER']
                else:
                        self.diff = diff_lower
                        self.init_pow = ezca['SETUPPERPOWER']

                ezca['INVERSE_FILTER_GAIN'] = 1
                ezca['FILTER_IN'] = self.diff                     # puts the difference in the filter and engages the filter
                ezca.switch('INVERSE_FILTER', 'FM1', 'ON')
                log('Turning on RH filter')
                log('Adjusting the RH power')
                
        def run(self):
                if (ezca['INVERSE_FILTER_OUTPUT'] + self.init_pow) < 0:
                        log('RH attempting to go to negative power')
                        adj_rh_power(0,0)
                else:
                        adj_rh_power(self.init_pow, ezca['INVERSE_FILTER_OUTPUT'])   # reads live output of the filter and assigns proper RH value
                        self.check_diff = abs(ezca['SETUPPERPOWER']-(self.init_power+self.diff))

                if self.check_diff > 0.95*(abs(self.diff)):
                        adj_rh_power(self.init_pow, self.diff)
                        return True


edges = [('NOMINAL_RH_INPUT', 'UNFILTERED_RH_INPUT'), 
    	 ('UNFILTERED_RH_INPUT', 'NOMINAL_RH_INPUT'),
    	 ('FILTER_RH_INPUT', 'NOMINAL_RH_INPUT'), 
	 ('NOMINAL_RH_INPUT', 'FILTER_RH_INPUT')]
